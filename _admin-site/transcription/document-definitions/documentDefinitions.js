import * as soliromUtils from "https://solirom.gitlab.io/admin-site/modules/utils/solirom-utils.js";
import solirom from "../index.js";
//import MiniEditorComponent from '../../solirom-mini-editor/solirom-mini-editor.js';
import LanguageSelectorComponent from "https://solirom.gitlab.io/admin-site/modules/solirom-language-selector/solirom-language-selector.js";

document.title = "Stamati, Dicționar";
document.querySelector("#title").textContent = "Stamati, Dicționar - transcriere și adnotare";

solirom.data.templates.transcriptionFile = `<ab xmlns="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude" type="aggregation"/>`;
const headwordFormTemplate = () =>
    `<t-form data-name="form" data-ns="http://www.tei-c.org/ns/1.0" slot="t-form-headword" type="headword">
        <t-orth data-name="orth" data-ns="http://www.tei-c.org/ns/1.0" slot="t-orth"></t-orth>
        <t-gram data-name="gram" data-ns="http://www.tei-c.org/ns/1.0" slot="t-gram" type="homonym_number"></t-gram>
        <t-gramgrp data-name="gramGrp" data-ns="http://www.tei-c.org/ns/1.0" slot="t-gramgrp"></t-gramgrp>
    </t-form>
    `
    ;

document.querySelector("data-editor").entryEditor.teianElementNameSuffixes = {
    "body": "entry",
    "form[type = 'headword']": "headword",
    "form[type = 'current-orth']": "current-orth",
};

const miniEditorStyles =
    `
    .mini-editor {
        background-color: white;
        padding: 3px;
        border: 1px solid black;
        width: auto;
        border-radius: 5px;
        overflow: auto;
        text-align: justify;
        width: 300px;
    }
    .mini-editor-large {
        height: 100px;
    }
`
    ;

solirom.data.templates.entryFileTemplate = data =>
    `<body xmlns="http://www.tei-c.org/ns/1.0" xml:id="${data.id}">
        <entry type="lemma">
            <form type="headword">
                <orth/>
                <gram type="homonym_number"/>
                <gramGrp/>
            </form>
            <def/>
            <form type="inflected"/>
        </entry>
        <note type="persons">
            <editor role="transcriber">${data.author}</editor>
        </note>
        <entryFree>
            <form type="current-orth">
                <orth/>
            </form>
            <idno type="lexicon"/>
        </entryFree>
    </body>`
    ;

const sicCorrStyle =
    `
    t-sic {
        background-color: orange;
    }
    t-corr {
        background-color: green;
    }
`;
const hiStyles =
    `
    *[data-name = 'hi'][rend = 'superscript'] {
        vertical-align: super;
        font-size: 70%;
    }
`;
const hiButtonsStyles =
    `
    button.hi[data-type = 'superscript'] > span {
        vertical-align: super;
        font-size: 70%;
    }
`;
const hiButtons =
    `
    <button class="hi" data-type="superscript" title="Textul selectat este redat la exponent"><span>1</span></button>
`;

solirom.data.entry = {
    "path": ""
};

teian.frameworkDefinition["t-body-entry-template"] =
    `
        <slot name="t-entry"></slot>
        <slot name="t-entryfree"></slot>
    `
    ;

teian.frameworkDefinition["t-entry-template"] =
    `
        <style>
            ${soliromUtils.awesomeButtonStyle}
            ${hiButtonsStyles}
            #entry-control {
                background-color: white;
                padding: 3px;
                border: 1px solid black;
                width: 500px;
                height: 400px;
                border-radius: 0 0 0 5px;
                font-size: 40px;
            }
        </style>
        <label for="entry-type-selector">Tip intrare</label>
        <select id="entry-type-selector" data-ref="@type">
            <option value="lemma">lemă</option>
            <option value="variant">variantă</option>
            <option value="flexionary form">formă flexionară</option>
            <option value="prefix">pref.</option>
            <option value="unknown">?</option>
        </select>
        <button id="uppercase-text-button" class="fa-button" title="Transformare litere în majuscule">&#xf062;</button>
        <button id="sic-button" title="Forma din text">sic</button>
        <button id="corr-button" title="Forma corectă">corr</button>
        <button id="clear-formatting" title="Eliminare formatare text">&#x2421;</button>
        ${hiButtons}
        <br/>
        <solirom-language-selector id="language-selector" data-ref="#text" data-languages="ro-x-accent-upcase-vowels,ro-x-accent-lowcase-vowels,ru-Cyrs,el-x-lowcase,el-x-upcase">
            <button data-character="'" slot="custom-toolbar">'</button>
            <button data-character="†" slot="custom-toolbar">†</button>
            <button data-character="§" slot="custom-toolbar">§</button>
            <button data-character="☼" slot="custom-toolbar">☼</button>
        </solirom-language-selector>
        <div id="entry-control" contenteditable="true" data-ref="node()" title="Intrare"></div>
    `
    ;

teian.frameworkDefinition["t-entryfree-template"] =
    `
        <style>
            label {
                display: block;
                margin-top: 10px;
            }
        </style>
        <label>Formă grafică pentru căutare</label>
        <slot name="t-form-current-orth"></slot>
    `
    ;
teian.frameworkDefinition["t-form-headword-template"] =
    `
        <style>
            :host(*) {
                margin-top: 10px;
                display: inline-block;
                padding: 2px;
            }
            #content {
                background: #adacac;
                display: grid;
                grid-template-columns: 330px 1fr;
                column-gap: 5px;
                border-radius: 5px;
            }
            ${soliromUtils.awesomeButtonStyle}
            #toolbar {
                width: 28px;
                padding-left: 22px;
            }
            #toolbar button {
                display: block;
                margin-top: 10px;
            }
        </style>
        <label>Cuvânt-titlu / număr de ordine omonim / categorii gramaticale</label>
        <div id="content">
            <div>
                <slot name="t-orth"></slot>
                <slot name="t-gram"></slot>
                <slot name="t-gramgrp"></slot>
            </div>
            <div id="toolbar">
                <button id="add-form-button" class="fa-button" title="Adăugare cuvânt-titlu">&#xf067;</button>
                <button id="delete-form-button" class="fa-button" title="Ștergere cuvânt-titlu">&#xf2ed;</button>
            </div>
        </div>
    `
    ;
teian.frameworkDefinition["t-form-current-orth-template"] =
    `
        <style>
            :host(*) {
                margin-top: 10px;
                display: inline-block;
                padding: 2px;
            }
            #content {
                background: #adacac;
                display: grid;
                grid-template-columns: 330px 1fr;
                column-gap: 5px;
                border-radius: 5px;
            }
            ${soliromUtils.awesomeButtonStyle}
            #toolbar {
                width: 50px;
            }
            #toolbar button {
                display: inline-block;
            }
        </style>
        <div id="content">
            <div>
                <slot name="t-orth"></slot>
                <slot name="t-gram"></slot>
                <slot name="t-gramgrp"></slot>
            </div>
            <div id="toolbar">
                <button id="add-form-button" class="fa-button" title="Adăugare cuvânt-titlu">&#xf067;</button>
                <button id="delete-form-button" class="fa-button" title="Ștergere cuvânt-titlu">&#xf2ed;</button>        
            </div>
        </div>
    `
    ;
teian.frameworkDefinition["t-orth-template"] =
    `
        <style>
            :host(*) {
                width: 350px;
                display: inline-block;
            }
            :host(:not(*[n])) #orth-mini-editor {
                border-radius: 5px 0 0 5px;
            }
            :host(:not(*[n])) #n-control {
                display: none;
            }
            #orth-mini-editor {
                background-color: white;
                padding: 3px;
                border: 1px solid black;
            }
            #orth-mini-editor {
                width: 300px;
                border-radius: 5px 0 0 0;
            }
            #n-control {
                width: 50px;
            }
            ${soliromUtils.awesomeButtonStyle}
            ${sicCorrStyle}
        </style>
        <div id="orth-mini-editor" contenteditable="true" data-ref="node()" title="Cuvânt-titlu"></div>
    `
    ;
teian.frameworkDefinition["t-gram-template"] =
    `
        <style>
            :host(*) {
                width: 350px;
                display: inline-block;
            }
            #n-control {
                background-color: white;
                padding: 3px;
                border: 1px solid black;
                width: 50px;
            }
            ${soliromUtils.awesomeButtonStyle}
            ${sicCorrStyle}
        </style>
        <div id="n-control" contenteditable="true" data-ref="node()" title="Nr. ordine omonim"></div>
    `
    ;
//<button id="add-current-orth-button" class="fa-button" title="Adăugare formă ortografică actuală">&#xf067;</button>
teian.frameworkDefinition["t-gramgrp-template"] =
    `
        <style>
            #gramGrp-control {
                background-color: white;
                padding: 3px;
                border: 1px solid black;
                width: 300px;
                border-radius: 0 0 0 5px;
            }
            ${sicCorrStyle}
            ${hiStyles}
        </style>
        <div id="gramGrp-control" contenteditable="true" data-ref="node()" title="Indicații gramaticale"></div>
    `
    ;
//<solirom-mini-editor id="language-selector" data-ref="#text" data-languages="ro-x-accent-upcase-vowels,ru-Cyrs"></solirom-mini-editor>

customElements.define("t-body-entry", class extends teian.divClass {
    constructor() {
        super();
    }
});
customElements.define("t-entry", class extends teian.formControlClass {
    constructor() {
        super();
    }
});
customElements.define("t-entryfree", class extends teian.divClass {
    constructor() {
        super();
    }
});
customElements.define("t-form-headword", class extends teian.divClass {
    constructor() {
        super();
    }
});
customElements.define("t-form-current-orth", class extends teian.divClass {
    constructor() {
        super();
    }
});
customElements.define("t-orth", class extends teian.formControlClass {
    constructor() {
        super();
    }
});
customElements.define("t-gram", class extends teian.formControlClass {
    constructor() {
        super();
    }
});
customElements.define("t-gramgrp", class extends teian.formControlClass {
    constructor() {
        super();
    }
});




teian.frameworkDefinition["t-ab-template"] =
    `
        <div class="list-group">
            <slot name="t-include"></slot>
        </div>
    `;
teian.frameworkDefinition["t-include-template"] =
    `
        <style>
            :host(*) {
                background-color: #ededeb;
                width:  160px;
                height: 25px;
                display: inline-block;
                margin-bottom: 5px;
                border-radius: 5px;
            }
            :host(*.selected-entry-reference) {
                background-color: #adacac;
            }
            :host(*[entry-type = 'variant']) {
                border-right: 5px solid #fac457;
            }
            .transcription-reference {
                display: inline-block;
                width: 100%;
                height: 25px;
                overflow: hidden;
                white-space: nowrap;
            }
            .transcription-reference > div {
                display: inline-block;
                position: relative;
                vertical-align: top;
            }
            .drag-handler {
                width: 29px;
                height: 25px;
                background: #807e7e url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI0MiIgaGVpZ2h0PSI0MiI+PGcgc3Ryb2tlPSIjRkZGIiBzdHJva2Utd2lkdGg9IjIuOSIgPjxwYXRoIGQ9Ik0xNCAxNS43aDE0LjQiLz48cGF0aCBkPSJNMTQgMjEuNGgxNC40Ii8+PHBhdGggZD0iTTE0IDI3LjFoMTQuNCIvPjwvZz4KPC9zdmc+') no-repeat center;
                cursor: move;
            }
            .transcription-detail {
                width: 160px;
                box-sizing: border-box;
                padding: 2px;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
            }
            :host(*[cert='validated']) .transcription-reference {
                border-left: 7px solid #f58d42;
            }
            :host(*[cert='reviewed']) .transcription-reference {
                border-left: 7px solid #5c9106;
            }

            :host(:last-of-type) {
                margin-bottom: 900px;
            }
        </style>
        <div class="transcription-reference">
            <div class="drag-handler"></div><div class="transcription-detail" data-ref="@label"></div>
        </div>
    `
    ;
customElements.define("t-ab", class extends teian.divClass {
    constructor() {
        super();
    }
});
customElements.define("t-include", class extends teian.formControlClass {
    constructor() {
        super();
    }
});