import Sortable from "https://solirom.gitlab.io/admin-site/modules/sortable/sortable.esm.js";
import * as soliromUtils from "https://solirom.gitlab.io/admin-site/modules/utils/solirom-utils.js";

/**
 * Gets an entry
 * @private
 * @return {string}
 */
solirom.actions._getEntry = async () => {
    const path = solirom.data.entry.path;
    let contents = await fetch(`${solirom.data.work.repoBaseUrl}${solirom.data.work.repoID}/repository/files/${encodeURIComponent(path)}/raw`, {
        "headers": {
            "PRIVATE-TOKEN": `${solirom.data.work.repoKey}`,
            'If-None-Match': ''
        }
    })
        .then(response => {
            if (response.status !== 200) {
                // TODO: When an entry is deleted, it has to be deleted from all the transcriptions it is referred within
                //alert("Eroare la încărcarea intrării.");                
                window.loadingSpinner.hide();

                alert("Nu se poate încărca intrarea.");

                throw new Error("Nu se poate încărca intrarea.");
            } else {
                return response.text();
            }
        });

    return contents;
};

const wrapText = (selection, contents) => {
    if (selection.rangeCount) {
        const range = selection.getRangeAt(0);
        const node = (new DOMParser()).parseFromString(contents, "text/html").querySelector("body > *").cloneNode(true);
        selection.deleteFromDocument();
        range.insertNode(node);

        return node;
    }
};

const importElementTemplate = (localName) => {
    return ns => {
        return attrs => {
            return contents => {
                return `<t-${localName} xmlns="http://www.w3.org/1999/xhtml" data-name="${localName}" data-ns="${ns}" slot="t-${localName}" ${attrs}>${contents}</t-${localName}>`;
            };
        };
    };
};

const getCorrectedTextContent = element => {
    const nodes = document.createTreeWalker(element, NodeFilter.SHOW_TEXT, null, null);

    var result = "";
    var node;
    while (node = nodes.nextNode()) {
        if (node.parentNode.getAttribute("data-name") === "sic") {
            continue;
        }
        result += node.textContent;
    }

    return result;
};

const entryReferenceTemplate = data => `<t-include data-name="xi:include" data-ns="http://www.w3.org/2001/XInclude" slot="t-include" href="${data.entryPath}" xpointer="/1/1" label="${data.label}" cert="${data.cert}" entry-type="lemma" class="list-group-item" draggable="true"></t-include>`;
const editorTemplate = data => `<t-editor data-name="editor" data-ns="http://www.tei-c.org/ns/1.0" slot="t-editor" role="${data.userRole}">${data.username}</t-editor>`;
const lexiconEntryTemplate = data => `<entryFree xmlns="http://www.tei-c.org/ns/1.0" xml:id="${data.id}"><form><orth n="${data.homonymNumber}">${data.headword}</orth><gramGrp>${data.gramGrp}</gramGrp></form></entryFree>`;
const choiceTemplate = data => `<t-choice data-name="choice" data-ns="http://www.tei-c.org/ns/1.0" slot="t-choice" resp="mailto:${data.username}">${data.sicElement}${data.corrElement}</t-choice>`;
const sicTemplate = data => `<t-sic data-name="sic" data-ns="http://www.tei-c.org/ns/1.0" slot="t-sic" resp="mailto:${data.username}">${data.textContent}</t-sic>`;
const corrTemplate = data => `<t-corr data-name="corr" data-ns="http://www.tei-c.org/ns/1.0" slot="t-corr" resp="mailto:${data.username}">${data.textContent}</t-corr>`;
const hiTemplate = importElementTemplate("hi")("http://www.tei-c.org/ns/1.0");

export default class DataEditorComponent extends HTMLElement {
    constructor() {
        super();

        this.attachShadow({ mode: 'open' });
        const shadowRoot = this.shadowRoot;
        shadowRoot.innerHTML =
            `
                <style>
                    #content {
                        background: white;
                        display: flex;
                        height: 990px;
                        gap: 10px;
                    }
                    #content, option {
                        font-size: 12px;
                    }
                    #transcription-editor-container {
                        width: 13vw;
                    }
                    #transcription-editor-container > * {
                        display: inline-block;
                    }
                    #transcription-editor-toolbar {
                        padding: 5px;
                        width: 190px
                    }
                    ${soliromUtils.awesomeButtonStyle}
                    #delete-entry-reference-button {
                        background-color: #98d964;
                    }
                    #delete-multiple-entries-button {
                        background-color: #968ced;
                    }
                    #transcription-editor {
                        --sc-width: 15vw;
                        --sc-height: 853px;
                    }
                    #entry-editor {
                        --sc-width: 35vw;
                        --sc-height: 900px;
                    }
                </style> 
                <div id="content">
                    <div id="transcription-editor-container">
                        <div id="transcription-editor-toolbar">
                            <button id="move-to-previous-page" class="fa-button" title="Trecere la pagina anterioară">&#9668;</button>
                            <output id="current-page-number"></output>
                            <button id="move-to-next-page" class="fa-button" title="Trecere la pagina următoare">&#9658;</button>
                            <button id="display-metadata-editor-button" class="fa-button" title="Întoarcere la numerotare pagini">&#xf03a;</button>                            
                            <br/>
                            <!--
                            <button id="add-entry-button" class="fa-button" title="Adăugare intrare">&#xf15b;</button>
                            <button id="duplicate-entry-button" class="fa-button" title="Duplicare ultima intrare din transcrierea anterioară">&#xf24d;</button>
                            <button id="delete-entry-button" class="fa-button" title="Ștergere intrare">&#xf2ed;</button>
                            <br/>
                            <button id="sort-entries-button" class="fa-button" title="Sortare intrări">&#xf162;</button>
                            <button id="delete-entry-reference-button" class="fa-button" title="Ștergere referință către intrare">&#xf2ed;</button>
                            <button id="delete-multiple-entries-button" class="fa-button" title="Ștergere a tuturor intrărilor">&#xf2ed;</button>
                            <br/>
                            -->
                            <label for="transcription-status-selector">Stare pagină</label>
                            <select id="transcription-status-selector">
                                <option value="unknown"></option>
                                <option value="validated">validată</option>
                                <option value="reviewed">revizuită</option>
                            </select>
                            <br/>
                            <label for="entries-number">Nr. intrări: </label><output id="entries-number">0</output>
                        </div>
                        <teian-editor id="transcription-editor"></teian-editor>                        
                    </div>
                    <teian-editor id="entry-editor">
                        <button slot="toolbar" id="save-entry-button" title="Salvare document" disabled="true">&#xf0c7;</button>
                        <label slot="toolbar" for="entry-status-selector">Stare intrare</label>
                        <select slot="toolbar" id="entry-status-selector" disabled="true">
                            <option value="unknown"></option>
                            <option value="validated">validată</option>
                            <option value="reviewed">revizuită</option>
                        </select> 
                        <button slot="toolbar" id="move-to-previous-entry" class="fa-button" title="Trecere la intrarea anterioară" disabled="true">&#9668;</button>
                        <button slot="toolbar" id="move-to-next-entry" class="fa-button" title="Trecere la intrarea următoare">&#9658;</button>    
                    </teian-editor> 
                </div>
            `
            ;

        this.transcriptionEditor = shadowRoot.querySelector("#transcription-editor");
        this.entryEditor = shadowRoot.querySelector("#entry-editor");
        this.currentPageNumberOutput = shadowRoot.querySelector("#current-page-number");
        solirom.controls.entriesNumberOutput = shadowRoot.querySelector("#entries-number");
        solirom.controls.transcriptionEditor = shadowRoot.querySelector("#transcription-editor");
        solirom.controls.transcriptionEditor.getContentsContainer().style.padding = "5px";
        this.entryStatusSelector = shadowRoot.querySelector("#entry-status-selector");
        this.selectedEntryReferenceElement = null;

        this.transcriptionStatusSelector = shadowRoot.querySelector("#transcription-status-selector");
        this.selectedPbElement = null;

        shadowRoot.addEventListener("click", async (event) => {
            const commit_email = document.querySelector("sc-user-roles").user.commit_email;
            if (commit_email === "") {
                alert("Selectați un utilizator.");

                return;
            }

            const composedTarget = event.composedPath()[0];

            // events related to document type
            if (composedTarget.matches("button#move-to-previous-page")) {
                const previousPbElement = this.selectedPbElement.previousElementSibling;
                this.currentPageNumberOutput.value = previousPbElement.getAttribute("n");
                this.editTranscription(previousPbElement);
                teian.actions.selectPageBreak(previousPbElement);
                this.entryEditor.reset();
                this.entryStatusSelector.value = "unknown";
                this.updateStatusOfMoveToEntryButtons();
            }

            if (composedTarget.matches("button#move-to-next-page")) {
                const nextPbElement = this.selectedPbElement.nextElementSibling;
                this.currentPageNumberOutput.value = nextPbElement.getAttribute("n");
                this.editTranscription(nextPbElement);
                teian.actions.selectPageBreak(nextPbElement);
                this.entryEditor.reset();
                this.entryStatusSelector.value = "unknown";
                this.updateStatusOfMoveToEntryButtons();
            }

            if (composedTarget.matches("button#move-to-previous-entry")) {
                this.setEntryForEditing(this.selectedEntryReferenceElement.previousElementSibling);
                this.updateStatusOfMoveToEntryButtons();
                this.editEntry();
            }

            if (composedTarget.matches("button#move-to-next-entry")) {
                this.setEntryForEditing(this.selectedEntryReferenceElement.nextElementSibling);
                this.updateStatusOfMoveToEntryButtons();
                this.editEntry();
            }

            if (composedTarget.matches(".transcription-reference, .transcription-reference *")) {
                this.setEntryForEditing(composedTarget.getRootNode().host);
                this.updateStatusOfMoveToEntryButtons();
            }

            // events related to document            
            if (composedTarget.matches("#display-metadata-editor-button")) {
                solirom.actions.displayMetadataEditor();
                this.selectedPbElement.scrollIntoView();
                document.body.scrollIntoView();
                this.transcriptionEditor.reset();
                this.entryEditor.reset();
                this.entryStatusSelector.value = "unknown";
            }

            if (composedTarget.matches("#add-entry-button")) {
                await this.addEntry();
                this.updateStatusOfMoveToEntryButtons();
            }

            if (composedTarget.matches("#duplicate-entry-button")) {
                await this.duplicateEntry();
                this.updateStatusOfMoveToEntryButtons();
            }

            if (composedTarget.matches("#delete-entry-button")) {
                let confirmDeleteEntry = confirm("Ștergeți intrarea?");

                if (confirmDeleteEntry) {
                    await this.deleteEntry();
                    this.updateStatusOfMoveToEntryButtons();
                }
            }

            if (composedTarget.matches("#delete-entry-reference-button")) {
                let confirmDeleteEntryReference = confirm("Ștergeți referința pentru intrare?");

                if (confirmDeleteEntryReference) {
                    await this.deleteEntryReference();
                    this.updateStatusOfMoveToEntryButtons();
                }
            }

            if (composedTarget.matches("#delete-multiple-entries-button")) {
                let confirmDeleteEntryReference = confirm("Ștergeți toate intrările?");

                if (confirmDeleteEntryReference) {
                    await this.deleteMultipleEntries();
                    this.updateStatusOfMoveToEntryButtons();
                }
            }

            if (composedTarget.matches("#sort-entries-button")) {
                this.sortEntries(composedTarget);
                this.updateStatusOfMoveToEntryButtons();
            }

            if (composedTarget.matches("#save-entry-button")) {
                await this.saveEntryAndTranscription();
            }

            if (composedTarget.matches("#uppercase-text-button")) {
                const miniEditor = this.focusedElement;
                if (miniEditor === null) {
                    alert("Selectați o casetă text.");

                    return;
                }
                var miniEditorTextContent = miniEditor.textContent;
                miniEditorTextContent = miniEditorTextContent.toUpperCase();
                miniEditor.textContent = miniEditorTextContent;
            }

            if (composedTarget.matches("#sic-button")) {
                const selection = (typeof this.focusedElement.getRootNode().getSelection === "function")
                    ? this.focusedElement.getRootNode().getSelection()
                    : document.getSelection();
                const contents = sicTemplate({ "username": commit_email, "textContent": selection.toString() });
                wrapText(selection, contents);
            }

            if (composedTarget.matches("#corr-button")) {
                const selection = (typeof this.focusedElement.getRootNode().getSelection === "function")
                    ? this.focusedElement.getRootNode().getSelection()
                    : document.getSelection();
                const contents = corrTemplate({ "username": commit_email, "textContent": selection.toString() });
                const corrElement = wrapText(selection, contents);
                const sicElement = corrElement.previousElementSibling;

                if (sicElement !== null && sicElement.getAttribute("data-name") === "sic") {
                    const sicElementString = sicElement.outerHTML;
                    sicElement.remove();
                    const templateString = choiceTemplate({ "username": commit_email, "sicElement": sicElementString, "corrElement": corrElement.outerHTML });
                    corrElement.insertAdjacentHTML("afterend", templateString);
                    corrElement.remove();
                }
            }

            if (composedTarget.matches("#clear-formatting")) {
                const miniEditor = this.focusedElement;
                const textContent = decodeURIComponent(miniEditor.textContent);
                miniEditor.innerHTML = textContent;
            }

            if (composedTarget.matches("button.hi, button.hi > span")) {
                const selection = (typeof this.focusedElement.getRootNode().getSelection === "function")
                    ? this.focusedElement.getRootNode().getSelection()
                    : document.getSelection();
                const contents = hiTemplate(`rend="${composedTarget.closest("button.hi").dataset.type}"`)(selection.toString());

                wrapText(selection, contents);
            }

            if (composedTarget.matches("#add-form-button")) {
                const currentFormElement = composedTarget.getRootNode().host;
                const currentFormElementParentName = currentFormElement.parentElement.dataset.name;

                switch (currentFormElementParentName) {
                    case "entry":
                        currentFormElement.insertAdjacentHTML("afterend", headwordFormTemplate);
                        break;
                    case "entryFree":
                        const newFormElement = currentFormElement.cloneNode(true);
                        currentFormElement.insertAdjacentElement("afterend", newFormElement);
                        break;
                }
            }

            if (composedTarget.matches("#delete-form-button")) {
                const currentFormElement = composedTarget.getRootNode().host;
                const currentFormElementParent = currentFormElement.parentElement;
                const formElements = currentFormElementParent.querySelectorAll("*[data-name = 'form']");
                if (formElements.length > 1) {
                    currentFormElement.remove();
                }
            }
        }, false);

        shadowRoot.addEventListener("dblclick", event => {
            const composedTarget = event.composedPath()[0];

            if (composedTarget.matches(".transcription-reference, .transcription-reference *")) {
                this.setEntryForEditing(composedTarget.getRootNode().host);
                this.editEntry();
            }
        }, false);

        shadowRoot.addEventListener("change", async event => {
            const target = event.composedPath()[0];

            if (target.matches("#transcription-status-selector")) {
                await this.saveTranscriptionAndMetadata(target.value);
            }

            if (target.matches("#entry-status-selector")) {
                window.loadingSpinner.show();
                this.selectedEntryReferenceElement.setAttribute("cert", target.value);
                await this.saveTranscription();
                window.loadingSpinner.hide();
            }
        }, false);

        shadowRoot.addEventListener("solirom-language-selector-value-changed", event => {
            const target = event.composedPath()[0];

            if (target.matches("#language-selector")) {
                const orthElement = target.getRootNode().host;
                orthElement.setAttribute("xml:lang", event.detail);
            }
        }, false);

        shadowRoot.addEventListener("solirom-language-selector-character-selected", event => {
            const target = event.composedPath()[0];

            if (target.matches("#language-selector")) {
                document.execCommand('insertHTML', false, event.detail);

                // const orthShadowRoot = target.getRootNode();
                // const orthElement = orthShadowRoot.querySelector("#orth-mini-editor");
                // orthElement.focus();
                // document.execCommand('insertHTML', false, event.detail);
            }
        }, false);

        this.entryEditor.addEventListener("teian-file-edited", event => {
            shadowRoot.querySelector("#save-entry-button").disabled = false;
        }, false);

        shadowRoot.addEventListener("mousedown", event => {
            const composedTarget = event.composedPath()[0];

            if (composedTarget.matches("div[contenteditable = 'true']")) {
                this.focusedElement = composedTarget;
            }


            /*             if (target.matches("#orth-mini-editor, #gramGrp-input")) {
                            const formElement = target.getRootNode().host.closest("*[data-name = 'form']");
                            const formType = formElement.getAttribute("type");
            
                            if (formType === "headword") {
                                const lemmaFormElement = formElement.closest("*[data-name = 'body']").querySelector("*[data-name = 'entryFree'][type = 'lemma'] *[data-name = 'form']");
                                const lemmaOrthElement = lemmaFormElement.querySelector("[data-name = 'orth']").shadowRoot.querySelector("#orth-mini-editor");
            
                                const orthElement = formElement.querySelector("[data-name = 'orth']");
            
                                const orthValue = orthElement.dataset.value;
                                console.log(orthValue);
                                const homonymValue = orthElement.getAttribute("n");
                                console.log(homonymValue);
                                
                                lemmaOrthElement.textContent = orthValue;
                                lemmaOrthElement.setAttribute("n", homonymValue);
                            }
                             
                        }  */
        }, false);
    }

    /**
     * Adds an entry and updates the transcription
     * @public
     * @return void
     */
    async addEntry() {
        const commit_email = document.querySelector("sc-user-roles").user.commit_email;
        if (commit_email === "") {
            alert("Selectați un utilizator.");

            return;
        }

        window.loadingSpinner.show();

        var entryId = await fetch("https://uuid.solirom.ro/cflr-" + solirom.data.work.id)
            .then(response => {
                if (response.status !== 200) {
                    window.loadingSpinner.hide();

                    alert("Nu se poate genera un identificator pentru intrare.");

                    throw new Error("Nu se poate genera un identificator pentru intrare.");
                } else {
                    return response.text();
                }
            });

        const transcriptionEditorShadowRoot = this.transcriptionEditor.shadowRoot;
        const newEntry = solirom.data.templates.entryFileTemplate({ "id": entryId, "author": commit_email });
        const newEntryPath = "entries/" + entryId + ".xml";

        const currentTranscription = this.transcriptionEditor.exportDataAsString();

        const newEntryReference = entryReferenceTemplate({ "entryPath": newEntryPath, "label": "", "cert": "unknown" });
        const currentEntryReferenceElement = transcriptionEditorShadowRoot.querySelector("*[data-name = 'xi:include'][class *= 'selected-entry-reference']");
        if (currentEntryReferenceElement === null) {
            transcriptionEditorShadowRoot.querySelector("*[data-name = 'ab']").insertAdjacentHTML("beforeend", newEntryReference);
        } else {
            currentEntryReferenceElement.insertAdjacentHTML("afterend", newEntryReference);
        }

        const xincludeElements = [...transcriptionEditorShadowRoot.querySelectorAll("*[data-name = 'xi:include']")];
        solirom.controls.entriesNumberOutput.textContent = xincludeElements.length;

        const newEntryReferenceElement = transcriptionEditorShadowRoot.querySelector("*[data-name = 'xi:include'][href = '" + newEntryPath + "']");

        this.highlightEntryReference(newEntryReferenceElement);
        this.selectedEntryReferenceElement = newEntryReferenceElement;

        solirom.data.entry.path = solirom.actions.composePath(["data", solirom.data.work.volumeNumber, solirom.data.repos.cflr.transcriptionsPath, newEntryPath], "/");

        const payload =
            `
            {
                "branch": "${solirom.data.work.repoDefaultBranch}",
                "commit_message": "${(new Date()).toISOString().split('.')[0] + ", " + commit_email}",
                "author_email": "${commit_email}",
                "actions": [
                    {
                        "action": "create",
                        "file_path": "${solirom.data.entry.path}",
                        "content": "${solirom.actions.b64EncodeUnicode(newEntry)}",
                        "encoding": "base64"                        
                    },
                    {
                        "action": "update",
                        "file_path": "${solirom.data.transcription.path}",
                        "content": "${solirom.actions.b64EncodeUnicode(this.transcriptionEditor.exportDataAsString())}",
                        "encoding": "base64"                        
                    }                    
                ]
            }
        `;
        await fetch(`${solirom.data.work.repoBaseUrl}${solirom.data.work.repoID}/repository/commits`, {
            "headers": {
                "PRIVATE-TOKEN": `${solirom.data.work.repoKey}`,
                "Content-Type": "application/json"
            },
            "method": "POST",
            "body": `${payload}`
        })
            .then(response => {
                if (response.status !== 201) {
                    window.loadingSpinner.hide();

                    this.entryEditor.reset();
                    this.transcriptionEditor.importData(currentTranscription);
                    this.selectedEntryReferenceElement = currentEntryReferenceElement;

                    alert("Intrarea nu a fost creată și transcrierea nu a fost salvată. Repetați operația.");
                }
            });

        this.entryEditor.importData(newEntry);
        this.entryStatusSelector.disabled = false;

        window.loadingSpinner.hide();
    }

    /**
     * Deletes the selected entry and updates the transcription
     * @public
     * @return void
     */
    async deleteEntry() {
        const commit_email = document.querySelector("sc-user-roles").user.commit_email;
        if (commit_email === "") {
            alert("Selectați un utilizator.");

            return;
        }

        window.loadingSpinner.show();

        //TODO: When an entry is deleted, it has to be deleted from all the transcriptions it is referred within
        // remove the transcription reference
        const currentTranscription = this.transcriptionEditor.exportDataAsString();
        this.selectedEntryReferenceElement.remove();

        const newTranscription = this.transcriptionEditor.exportDataAsString();
        const payload =
            `
            {
                "branch": "${solirom.data.work.repoDefaultBranch}",
                "commit_message": "${(new Date()).toISOString().split('.')[0] + ", " + commit_email}",
                "author_email": "${commit_email}",
                "actions": [
                    {
                        "action": "delete",
                        "file_path": "${solirom.data.entry.path}"                        
                    },
                    {
                        "action": "update",
                        "file_path": "${solirom.data.transcription.path}",
                        "content": "${solirom.actions.b64EncodeUnicode(newTranscription)}",
                        "encoding": "base64"                        
                    }                    
                ]
            }
        `;
        await fetch(`${solirom.data.work.repoBaseUrl}${solirom.data.work.repoID}/repository/commits`, {
            "headers": {
                "PRIVATE-TOKEN": `${solirom.data.work.repoKey}`,
                "Content-Type": "application/json"
            },
            "method": "POST",
            "body": `${payload}`
        })
            .then(response => {
                if (response.status !== 201) {
                    window.loadingSpinner.hide();

                    // add back the transcription reference
                    this.transcriptionEditor.importData(currentTranscription);

                    alert("Intrarea nu poate fi ștearsă.");
                }
            });

        //reset the entry editor
        this.entryEditor.reset();
        this.entryStatusSelector.value = "unknown";

        const xincludeElements = [...solirom.controls.transcriptionEditor.shadowRoot.querySelectorAll("*[data-name = 'xi:include']")];
        solirom.controls.entriesNumberOutput.textContent = xincludeElements.length;

        window.loadingSpinner.hide();
    }

    /**
     * Deletes the selected entry reference || TODO: this function will be removed when
     * the data will have the new structure
     * @public
     * @return void
     */
    async deleteEntryReference() {
        window.loadingSpinner.show();

        // check if an entry reference is selected
        if (this.selectedEntryReferenceElement === null) {
            window.loadingSpinner.hide();

            alert("Pentru a șterge o intrare duplicată, trebuie selectată una.");

            return;
        }

        const currentTranscription = this.transcriptionEditor.exportDataAsString();
        this.selectedEntryReferenceElement.remove();

        // save the transcription
        await this.saveTranscription();

        //reset the entry editor
        this.entryEditor.reset();
        this.entryStatusSelector.value = "unknown";

        // update the counter of entry references
        const xincludeElements = [...this.transcriptionEditor.shadowRoot.querySelectorAll("*[data-name = 'xi:include']")];
        solirom.controls.entriesNumberOutput.textContent = xincludeElements.length;

        window.loadingSpinner.hide();
    }

    /**
     * Deletes all the entries and updates the transcription
     * TODO: this function will be removed when CLRE will be transcribed
     * @public
     * @return void
     */
    async deleteMultipleEntries() {
        const commit_email = document.querySelector("sc-user-roles").user.commit_email;
        if (commit_email === "") {
            alert("Selectați un utilizator.");

            return;
        }

        window.loadingSpinner.show();

        const currentTranscription = this.transcriptionEditor.exportDataAsString();

        let entryHrefs = [...this.transcriptionEditor.shadowRoot.querySelectorAll("*[data-name = 'xi:include']")].map(element => element.getAttribute("href"));
        entryHrefs = [...new Set(entryHrefs)];
        this.transcriptionEditor.getContentsContainer().querySelector("*[data-name = 'ab']").innerHTML = "";

        const newTranscription = this.transcriptionEditor.exportDataAsString();

        const entryRelatedActions = entryHrefs.map((entryHref) => {
            const entryPath = solirom.actions.composePath(["data", solirom.data.work.volumeNumber, solirom.data.repos.cflr.transcriptionsPath, entryHref], "/");

            return `
                {
                    "action": "delete",
                    "file_path": "${entryPath}"                        
                },			
            `;
        }).join("");
        const payload =
            `
            {
                "branch": "${solirom.data.work.repoDefaultBranch}",
                "commit_message": "${(new Date()).toISOString().split('.')[0] + ", " + commit_email}",
                "author_email": "${commit_email}",
                "actions": [
                    ${entryRelatedActions}
                    {
                        "action": "update",
                        "file_path": "${solirom.data.transcription.path}",
                        "content": "${solirom.actions.b64EncodeUnicode(newTranscription)}",
                        "encoding": "base64"                        
                    }                    
                ]
            }
        `;
        await fetch(`${solirom.data.work.repoBaseUrl}${solirom.data.work.repoID}/repository/commits`, {
            "headers": {
                "PRIVATE-TOKEN": `${solirom.data.work.repoKey}`,
                "Content-Type": "application/json"
            },
            "method": "POST",
            "body": `${payload}`
        })
            .then(response => {
                if (response.status !== 201) {
                    window.loadingSpinner.hide();

                    this.transcriptionEditor.importData(currentTranscription);

                    alert("Eroare la ștergerea intrărilor din transcriere. Repetați operația.");
                }
            });

        solirom.controls.entriesNumberOutput.textContent = 0;
        this.selectedEntryReferenceElement = null;
        this.entryEditor.reset();
        this.entryStatusSelector.value = "unknown";
        this.entryStatusSelector.disabled = true;

        window.loadingSpinner.hide();
    };

    /**
     * Duplicates the last entry reference of the previous transcription
     * @private
     * @return {string}
     */
    async duplicateEntry() {
        const currentTranscriptionPath = this.selectedPbElement.getAttribute("corresp");
        const previousTranscriptionReference = document.querySelector("teian-editor#metadata-editor").shadowRoot.querySelector("*[data-name = 'pb'][corresp = '" + currentTranscriptionPath + "']").previousElementSibling;

        if (previousTranscriptionReference !== null) {
            window.loadingSpinner.show();

            const transcriptionEditorShadowRoot = solirom.controls.transcriptionEditor.shadowRoot;
            const previousTranscriptionPath = solirom.actions.composePath(["data", solirom.data.work.volumeNumber, previousTranscriptionReference.getAttribute("corresp")], "/");
            const transcriptionResult = await solirom.actions._getTranscription(previousTranscriptionPath);
            const previousTranscription = (new DOMParser()).parseFromString(transcriptionResult, "application/xml").documentElement;
            const lastEntryReference = previousTranscription.querySelector("*|include:last-of-type");
            const lastEntryReferenceHref = lastEntryReference.getAttribute("href");

            if (transcriptionEditorShadowRoot.querySelector(`*[data-name = 'xi:include'][href = '${lastEntryReferenceHref}']`) !== null) {
                window.loadingSpinner.hide();
                alert("Ultima intrare de pe pagina anterioară a fost duplicată deja.");

                return;
            }

            const newEntryReference = entryReferenceTemplate({ "entryPath": lastEntryReferenceHref, "label": lastEntryReference.getAttribute("label"), "cert": lastEntryReference.getAttribute("cert") });
            transcriptionEditorShadowRoot.querySelector("*[data-name = 'ab']").insertAdjacentHTML("afterbegin", newEntryReference);

            // update the counter for number of entries
            const xincludeElements = [...transcriptionEditorShadowRoot.querySelectorAll("*[data-name = 'xi:include']")];
            solirom.controls.entriesNumberOutput.textContent = xincludeElements.length;

            // save the transcription
            await this.saveTranscription();
            window.loadingSpinner.hide();
        }
    }

    /**
     * Edits the selected entry
     * @public
     * @return void
     */
    async editEntry(selectedEntryReferenceElement) {
        window.loadingSpinner.show();

        this.entryStatusSelector.value = this.selectedEntryReferenceElement.getAttribute("cert");
        const entry = await solirom.actions._getEntry();

        this.entryEditor.importData(entry);

        window.loadingSpinner.hide();
    }

    /**
     * Edits the selected transcription
     * @public
     * @return void
     */
    async editTranscription(selectedPbElement) {
        window.loadingSpinner.show();

        // check for previous, respectively next page
        const previousPageButton = this.shadowRoot.querySelector("button#move-to-previous-page");
        const nextPageButton = this.shadowRoot.querySelector("button#move-to-next-page");
        if (selectedPbElement.previousElementSibling === null) {
            previousPageButton.disabled = true;
        } else {
            previousPageButton.disabled = false;
        }
        if (selectedPbElement.nextElementSibling === null) {
            nextPageButton.disabled = true;
        } else {
            nextPageButton.disabled = false;
        }

        // load the scan, if any
        const scanName = selectedPbElement.getAttribute("facs");
        solirom.data.scan.name = scanName;
        solirom.actions.updateImageViewerURL(scanName);

        // display the transcription editor
        solirom.actions.displayDataEditor();

        // load the transcription
        this.selectedPbElement = selectedPbElement;
        this.transcriptionStatusSelector.value = selectedPbElement.getAttribute("cert");

        var transcriptionPath = selectedPbElement.getAttribute("corresp");
        solirom.data.transcription.path = solirom.actions.composePath(["data", solirom.data.work.volumeNumber, transcriptionPath], "/");

        const transcription = await solirom.actions._getTranscription(solirom.data.transcription.path);
        solirom.controls.transcriptionEditor.importData(transcription);

        const abElement = solirom.controls.transcriptionEditor.shadowRoot.querySelector("*[data-name = 'ab']");
        abElement.classList.add("list-group");
        const xincludeElements = [...solirom.controls.transcriptionEditor.shadowRoot.querySelectorAll("*[data-name = 'xi:include']")];
        solirom.controls.entriesNumberOutput.textContent = xincludeElements.length;
        xincludeElements.forEach(
            includeElement => {
                includeElement.classList.add("list-group-item");
                const transcriptionReference = includeElement.shadowRoot.querySelector(".transcription-reference");
                transcriptionReference.setAttribute("title", includeElement.getAttribute("label").replace(/&lt;\/?sup&gt;/g, "").replace(/&nbsp;/g, " "));
            }
        );
        Sortable.create(abElement, {
            animation: 350,
            onEnd: async () => {
                window.loadingSpinner.show();
                const transcriptionEditor = document.querySelector("data-editor");
                await transcriptionEditor.saveTranscription();
                transcriptionEditor.updateStatusOfMoveToEntryButtons();
                window.loadingSpinner.hide();
            }
        });
        window.loadingSpinner.hide();
    }

    /**
     * Saves the selected entry, along with the transcription
     * @public
     * @return void
     */
    async saveEntryAndTranscription() {
        const commit_email = document.querySelector("sc-user-roles").user.commit_email;
        if (commit_email === "") {
            alert("Selectați un utilizator.");

            return;
        }
        const userRole = document.querySelector("sc-user-roles").role;

        window.loadingSpinner.show();
        const currentEntry = this.entryEditor.exportDataAsString();
        const currentTranscription = this.transcriptionEditor.exportDataAsString();

        // process the transcription
        let transcriptionLabel = [...this.entryEditor.shadowRoot.querySelectorAll("*[data-name = 'form'][type = 'headword']")].map(formElement => {
            const orthElement = formElement.querySelector("*[data-name = 'orth']");
            const gramElement = formElement.querySelector("*[data-name = 'gram']");
            const gramGrpElement = formElement.querySelector("*[data-name = 'gramGrp']");

            const homonymNumber = getCorrectedTextContent(gramElement);
            const headword = getCorrectedTextContent(orthElement).split(/\s+/).map((item, index) => {
                if (index === 0) {
                    if (item.endsWith(',') || item.endsWith('-')) {
                        item = item.replace(/([,|-])$/, `<sup>${homonymNumber}</sup>$1`);
                    } else if (homonymNumber.trim() !== "") {
                        item = `${item}<sup>${homonymNumber}</sup>`;
                    }
                }

                return item;
            }).filter(Boolean).join(" ");
            let gramGrp = getCorrectedTextContent(gramGrpElement);
            gramGrp = gramGrp.replace(/([I|V])(a)$/, "$1<sup>$2</sup>");

            return headword + " " + gramGrp;
        }).filter(Boolean).join(" ");

        transcriptionLabel = transcriptionLabel.replace(/(&lt;[^&gt;]+&gt;|&lt;[^&gt;]>|&lt;\/[^&gt;]>)/g, "").replace(/&nbsp;/g, " ").trim();
        // if transcriptionLabel is empty, use the entry's id
        if (transcriptionLabel === "") {
            transcriptionLabel = this.entryEditor.shadowRoot.querySelector("*[data-name = 'body']").getAttribute("xml\:id");
        }
        const entryType = this.entryEditor.shadowRoot.querySelector("*[data-name = 'entry']").getAttribute("type");
        const selectedEntryReferenceElement = this.transcriptionEditor.shadowRoot.querySelector("*[data-name = 'xi:include'][class *= 'selected-entry-reference']");
        const transcriptionReference = selectedEntryReferenceElement.shadowRoot.querySelector(".transcription-reference");
        const transcriptionDetail = selectedEntryReferenceElement.shadowRoot.querySelector(".transcription-detail");
        transcriptionReference.setAttribute("title", transcriptionLabel.replace(/<\/?sup>/g, ""));
        transcriptionDetail.innerHTML = transcriptionLabel;

        selectedEntryReferenceElement.setAttribute("entry-type", entryType);
        transcriptionDetail.dispatchEvent(new Event("input", { bubbles: true, composed: true }));

        const editorElements = [...this.entryEditor.getContentsContainer()
            .querySelectorAll("*[data-name = 'editor'][role = '" + userRole + "']")]
            .filter(editorElement => editorElement.textContent === commit_email);
        if (editorElements.length === 0) {
            const editorAsString = editorTemplate({ "username": commit_email, "userRole": userRole });
            const personsElement = this.entryEditor.getContentsContainer().querySelector("*[data-name = 'note'][type = 'persons']");
            personsElement.insertAdjacentHTML("afterbegin", editorAsString);
        }
        const newTranscription = this.transcriptionEditor.exportDataAsString();

        let errorMessage = "Intrarea și transcrierea nu au fost salvate. Repetați salvarea.";

        const payload =
            `
            {
                "branch": "${solirom.data.work.repoDefaultBranch}",
                "commit_message": "${(new Date()).toISOString().split('.')[0] + ", " + commit_email}",
                "author_email": "${commit_email}",
                "actions": [
                    {
                        "action": "update",
                        "file_path": "${solirom.data.entry.path}",
                        "content": "${solirom.actions.b64EncodeUnicode(currentEntry)}",
                        "encoding": "base64"
                    },
                    {
                        "action": "update",
                        "file_path": "${solirom.data.transcription.path}",
                        "content": "${solirom.actions.b64EncodeUnicode(newTranscription)}",
                        "encoding": "base64"
                    }
                ]
            }
        `;
        await fetch(`${solirom.data.work.repoBaseUrl}${solirom.data.work.repoID}/repository/commits`, {
            "headers": {
                "PRIVATE-TOKEN": `${solirom.data.work.repoKey}`,
                "Content-Type": "application/json"
            },
            "method": "POST",
            "body": `${payload}`
        })
            .then(response => {
                if (response.status !== 201) {
                    window.loadingSpinner.hide();

                    this.transcriptionEditor.importData(currentTranscription);

                    alert(errorMessage);

                    throw new Error(errorMessage);
                }
            });

        window.loadingSpinner.hide();
    }

    /**
     * Saves the selected transcription
     * @public
     * @return void
     */
    async saveTranscription() {
        const commit_email = document.querySelector("sc-user-roles").user.commit_email;
        if (commit_email === "") {
            alert("Selectați un utilizator.");

            return;
        }

        const currentTranscription = this.transcriptionEditor.exportDataAsString();
        const payload =
            `
            {
                "branch": "${solirom.data.work.repoDefaultBranch}",
                "commit_message": "${(new Date()).toISOString().split('.')[0] + ", " + commit_email}",
                "author_email": "${commit_email}",
                "content": "${solirom.actions.b64EncodeUnicode(currentTranscription)}",
                "encoding": "base64"
            }
        `;

        await fetch(`${solirom.data.work.repoBaseUrl}${solirom.data.work.repoID}/repository/files/${encodeURIComponent(solirom.data.transcription.path)}`, {
            "headers": {
                "PRIVATE-TOKEN": `${solirom.data.work.repoKey}`,
                "Content-Type": "application/json"
            },
            "method": "PUT",
            "body": `${payload}`
        })
            .then(response => {
                if (response.status !== 200) {
                    window.loadingSpinner.hide();

                    this.transcriptionEditor.importData(currentTranscription);

                    alert("Transcrierea nu a fost salvată. Repetați operația.");
                }
            });
    }

    /**
     * Saves the modified transcription and the work metadata
     * @public
     * @return void
     */
    async saveTranscriptionAndMetadata(transcriptionStatus) {
        const commit_email = document.querySelector("sc-user-roles").user.commit_email;
        if (commit_email === "") {
            alert("Selectați un utilizator.");

            return;
        }

        window.loadingSpinner.show();

        const metadataEditor = document.querySelector("teian-editor#metadata-editor");

        // get the old state
        const currentTranscription = this.transcriptionEditor.exportDataAsString();
        const currentMetadata = metadataEditor.exportDataAsString();
        const currentTranscriptionStatus = this.selectedPbElement.getAttribute("cert");

        console.log(transcriptionStatus);

        // set the new state
        console.log(this.selectedPbElement.getAttribute("cert"));
        this.selectedPbElement.setAttribute("cert", transcriptionStatus);
        console.log(this.selectedPbElement.getAttribute("cert"));
        let entryReferences = [...this.transcriptionEditor.shadowRoot.querySelectorAll(`*[data-name = 'xi:include']:not([cert = '${transcriptionStatus}'])`)];
        entryReferences.forEach((entryReference) => {
            // == to be used when the entries will have status
            //this.setEntryForEditing(entryReference);
            //const entry = await solirom.actions._getEntry();
            // == end of to be used when the entries will have status

            entryReference.setAttribute("cert", transcriptionStatus);
        });

        // save the new state
        const metadataFilePath = solirom.actions.composePath(["data", solirom.data.work.volumeNumber, "index.xml"], "/");
        console.log(metadataFilePath);
        const payload =
            `
            {
                "branch": "${solirom.data.work.repoDefaultBranch}",
                "commit_message": "${(new Date()).toISOString().split('.')[0] + ", " + commit_email}",
                "author_email": "${commit_email}",
                "actions": [
                    {
                        "action": "update",
                        "file_path": "${solirom.data.transcription.path}",
                        "content": "${solirom.actions.b64EncodeUnicode(this.transcriptionEditor.exportDataAsString())}",
                        "encoding": "base64"                        
                    },                     
                    {
                        "action": "update",
                        "file_path": "${metadataFilePath}",
                        "content": "${solirom.actions.b64EncodeUnicode(metadataEditor.exportDataAsString())}",
                        "encoding": "base64"                        
                    }                   
                ]
            }
        `;

        await fetch(`${solirom.data.work.repoBaseUrl}${solirom.data.work.repoID}/repository/commits`, {
            "headers": {
                "PRIVATE-TOKEN": `${solirom.data.work.repoKey}`,
                "Content-Type": "application/json"
            },
            "method": "POST",
            "body": `${payload}`
        })
            .then(response => {
                if (response.status !== 201) {
                    window.loadingSpinner.hide();

                    // restore the old state
                    this.transcriptionEditor.importData(currentTranscription);
                    metadataEditor.importData(currentMetadata);
                    this.selectedPbElement.setAttribute("cert", currentTranscriptionStatus);
                    this.transcriptionStatusSelector.value = currentTranscriptionStatus;

                    alert("Transcrierea și metadatele nu au fost salvate. Repetați operația.");
                }
            });

        window.loadingSpinner.hide();
    }

    /**
     * Updates the status of the buttons for moving to previous / next entry
     * @private
     * @return void
     */
    updateStatusOfMoveToEntryButtons() {
        if (this.selectedEntryReferenceElement === null) {
            return;
        }

        const previousEntryButton = this.shadowRoot.querySelector("button#move-to-previous-entry");
        const nextEntryButton = this.shadowRoot.querySelector("button#move-to-next-entry");
        if (this.selectedEntryReferenceElement.previousElementSibling === null) {
            previousEntryButton.disabled = true;
        } else {
            previousEntryButton.disabled = false;
        }
        if (this.selectedEntryReferenceElement.nextElementSibling === null) {
            nextEntryButton.disabled = true;
        } else {
            nextEntryButton.disabled = false;
        }
    }

    /**
     * Sort the entries' references in a transcription
     * @public
     * @return void
     */
    async sortEntries(element) {
        window.loadingSpinner.show();

        const transcriptionEditor = this.transcriptionEditor;
        const entryReferencesContainer = transcriptionEditor.getContentsContainer().querySelector("*[data-name = 'ab']");
        const sortedEntryReferences = [...transcriptionEditor.getContentsContainer().querySelectorAll("*[data-name = 'xi:include']")].sort((a, b) => {
            const aLabel = a.getAttribute("label");
            const bLabel = b.getAttribute("label");

            if (aLabel < bLabel) {
                return -1;
            }
            if (aLabel > bLabel) {
                return 1;
            }

            return 0;
        });

        sortedEntryReferences.forEach((item) => {
            entryReferencesContainer.appendChild(item);
        });

        // save the transcription
        await this.saveTranscription();

        window.loadingSpinner.hide();
    }

    highlightEntryReference(currentEntryReference) {
        [...currentEntryReference.parentNode.querySelectorAll("*[data-name = 'xi:include']")].forEach(
            entryReference => entryReference.classList.remove("selected-entry-reference")
        );
        currentEntryReference.classList.add("selected-entry-reference");
    }

    setEntryForEditing(selectedEntryReferenceElement) {
        this.selectedEntryReferenceElement = selectedEntryReferenceElement;
        this.entryStatusSelector.disabled = false;
        this.highlightEntryReference(selectedEntryReferenceElement);
        solirom.data.entry.path = solirom.actions.composePath(["data", solirom.data.work.volumeNumber, solirom.data.repos.cflr.transcriptionsPath, selectedEntryReferenceElement.getAttribute("href")], "/");
    }

    reset() {
        solirom.controls.transcriptionEditor.reset();
        this.entryEditor.reset();
        this.entryStatusSelector.value = "unknown";

    }
};

window.customElements.define("data-editor", DataEditorComponent);

// save the entry in lexicon
/*         const editorContents = this.entryEditor.getContentsContainer();
        const lemmaOrthElement = editorContents.querySelector("*[data-name = 'entryFree'][type = 'lemma'] *[data-name = 'orth']");
        const headword = lemmaOrthElement.dataset.value;

        const homonymNumber = editorContents.querySelector("*[data-name = 'orth']").getAttribute("n");
        const gramGrp = editorContents.querySelector("*[data-name = 'gramGrp']").dataset.value;
        const lexiconEntryIdElement = editorContents.querySelector("*[data-name = 'idno'][type = 'lexicon']");
        var lexiconEntryId = lexiconEntryIdElement.dataset.value;

        var lexiconEntryAsString = lexiconEntryTemplate({"id": lexiconEntryId, "headword": headword, "homonymNumber": homonymNumber, "gramGrp": gramGrp});

        if (lexiconEntryId === "") {
            try {
                lexiconEntryId = await fetch("https://uuid.solirom.ro/lexicon").then(response => response.text());
            } catch (error) {
                console.error(error);
                window.loadingSpinner.hide();
                alert("Nu se poate genera un identificator pentru intrarea din lexicon.");

                return;
            }
            lexiconEntryAsString = lexiconEntryTemplate({"id": lexiconEntryId, "headword": headword, "homonymNumber": homonymNumber, "gramGrp": gramGrp});

            try {
                await solirom.data.repos.cflr.xlient({
                    method: "PUT",
                    path: lexiconEntryId,
                    owner: "solirom",
                    repo: "lexicon",
                    content: solirom.actions.b64EncodeUnicode(lexiconEntryAsString),
                    "message": (new Date()).toISOString().split('.')[0] + ", " + username,
                    "committer": {
                        "email": username,
                        "name": username
                    },
                });    
            } catch (error) {
                console.error(error);
                window.loadingSpinner.hide();
                alert("Nu se poate salva intrarea în lexicon.");

                return;
            }

            lexiconEntryIdElement.dataset.value = lexiconEntryId;
        } else {
            // get the SHA of the lexicon entry
            var lexiconEntrySha = await solirom.data.repos.cflr.xlient({
                method: "HEAD",
                path: lexiconEntryId,
                owner: "solirom",
                repo: "lexicon",
                headers: {
                    'If-None-Match': ''
                    }	
            });
            lexiconEntrySha = lexiconEntrySha.headers.etag;
            lexiconEntrySha = lexiconEntrySha.replace("W/", "").replaceAll('"', '').trim();

            // save the lexicon entry
            try {
                await solirom.data.repos.cflr.xlient({
                    method: "PUT",
                    path: lexiconEntryId,
                    owner: "solirom",
                    repo: "lexicon",
                    sha: lexiconEntrySha,
                    content: solirom.actions.b64EncodeUnicode(lexiconEntryAsString),
                    "message": (new Date()).toISOString().split('.')[0] + ", " + username,
                    "committer": {
                        "email": username,
                        "name": username
                    },
                });    
            } catch (error) {
                console.error(error);
                window.loadingSpinner.hide();
                alert("Nu se poate salva intrarea în lexicon.");

                return;
            }
        } */
