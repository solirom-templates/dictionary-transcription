import Sortable from "https://solirom.gitlab.io/admin-site/modules/sortable/sortable.esm.js";
import * as Messages from "./messages.js";

solirom.data.work = {
	"volumeNumber": "",
	"textSection": "body",
	"repoID": "",
	"repoBaseUrl": "",
	"repoKey": ""
};

// load the repo data
let repoData = await fetch(new URL("repo.json", location.href)).then(data => data.json());
[solirom.data.work.repoID, solirom.data.work.repoBaseUrl] = [repoData.id, repoData.baseUrl];

solirom.actions.loadWork = async () => {
	let repoDefaultBranch = await fetch(`${solirom.data.work.repoBaseUrl}${solirom.data.work.repoID}/repository/branches`, {
		"headers": {
			"PRIVATE-TOKEN": `${solirom.data.work.repoKey}`
		}
	})
		.then(response => {
			if (response.status !== 200) {
				alert(Messages.errorRepoBranch);

				throw new Error(Messages.errorRepoBranch);
			} else {
				return response.json();
			}
		});
	repoDefaultBranch = repoDefaultBranch.find(item => item.default === true).name;

	solirom.data.work.repoDefaultBranch = repoDefaultBranch;

	let entries;
	entries = await fetch(`${solirom.data.work.repoBaseUrl}${solirom.data.work.repoID}/repository/tree?path=data/&pagination=keyset&per_page=100`, {
		"headers": {
			"PRIVATE-TOKEN": `${solirom.data.work.repoKey}`
		}
	})
		.then(response => {
			if (response.status !== 200) {
				alert(Messages.errorVolumes);

				throw new Error(Messages.errorVolumes);
			} else {
				return response.json();
			}
		});
	let volumeNumbers = entries.filter((item) => { return item.name.match(/\d+/g) }).map((item) => item.name);

	solirom.data.work.volumeNumber = "";
	document.querySelector("teian-editor#metadata-editor").reset();
	solirom.actions.resetScanViewer();

	if (volumeNumbers.length === 0) {
		let contents = await fetch(`${solirom.data.work.repoBaseUrl}${solirom.data.work.repoID}/repository/files/${encodeURIComponent("data/index.xml")}/raw`, {
			"headers": {
				"PRIVATE-TOKEN": `${solirom.data.work.repoKey}`,
				'If-None-Match': ''
			}
		})
			.then(response => {
				if (response.status !== 200) {
					alert(Messages.errorGeneralMetadata);

					throw new Error(Messages.errorGeneralMetadata);
				} else {
					return response.text();
				}
			});

		const metadataEditor = document.querySelector("teian-editor#metadata-editor");
		metadataEditor.importData(contents);
		document.querySelector("#add-scan").disabled = false;

		const metadataEditorShadowRoot = metadataEditor.shadowRoot;
		[...metadataEditorShadowRoot.querySelectorAll("#content *[data-name = 'text'] > *")].forEach((section) => section.style.display = "none");
		metadataEditorShadowRoot.querySelector("#content *[data-name = '" + solirom.data.work.textSection + "']").style.display = "inline";

		[...metadataEditorShadowRoot.querySelectorAll("*[data-name = 'front'], *[data-name = 'back']")].forEach((section => {
			section.classList.add("list-group");
			[...section.querySelectorAll("*[data-name = 'pb']")].forEach((pb) => {
				pb.classList.add("list-group-item");
			});
			Sortable.create(section, {
				animation: 350
			});
		}));
	} else {
		const volumeSelector = document.querySelector("#volume-selector");
		volumeNumbers = volumeNumbers.map(
			(item) => {
				return solirom.data.templates.volumeSelectorOption({ "label": item, "value": item });
			}).filter(Boolean).join("");
		volumeNumbers = solirom.data.templates.volumeSelectorOption({ "label": "", "value": "" }) + volumeNumbers;

		volumeSelector.style.display = "inline";
		document.querySelector("label[for = 'volume-selector']").style.display = "inline";

		volumeSelector.innerHTML = volumeNumbers;
	}

	setTimeout(() => document.querySelector("#save-button").disabled = true, 100);
};

let personalAccessTokenComponent = document.querySelector("sc-personal-access-token");
let userRolesComponent = document.querySelector("sc-user-roles");

let users = await fetch(new URL("users.json", location.href)).then(response => response.json());
userRolesComponent.users = users;

if (personalAccessTokenComponent.pat !== "" && userRolesComponent.role !== "") {
	solirom.data.work.repoKey = personalAccessTokenComponent.pat;
	solirom.actions.loadWork();
}

solirom.data.repos = {
	"https://gitlab.com/api/v4/projects/": "glpat-DxkypfC9NNCUMkCytqeq",
	"cflr": {
		"transcriptionsPath": "transcriptions"
	}
};
solirom.data.transcription = {
	"path": ""
};
solirom.data.templates = {
	"volumeSelectorOption": data => `
		<option xmlns="http://www.w3.org/1999/xhtml" value="${data.value}">${data.label}</option>
	`,
	"pb": data => `
		<t-pb xmlns="http://www.w3.org/1999/xhtml" data-name="pb" data-ns="http://www.tei-c.org/ns/1.0" slot="t-pb"  n="" facs="${data.facs}" cert="unknown" corresp="${data.transcriptionPath}"></t-pb>
	`,
	"img": data => `<img xmlns="http://www.w3.org/1999/xhtml" id="scan" src="${data}"/>`
};
solirom.data.scan = {
	"name": ""
};
solirom.data.messages = {
	"legalFileSize": ""
};

solirom.events.fileSave = new CustomEvent("fileSave");
solirom.events.fileDelete = new CustomEvent("fileDelete");

// === this is part of DocumentTemplates (dictionary-templates.js)
solirom.data.templates.transcriptionFile = `<ab xmlns="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude" type="aggregation"/>`;
// ===

document.addEventListener("sc-personal-access-token:user-data", async event => {
	userRolesComponent.user = event.detail;
	solirom.data.work.repoKey = personalAccessTokenComponent.pat;
});

document.addEventListener("sc-user-roles:selected-role", async event => {
	solirom.actions.loadWork();
});

document.addEventListener("fileSave", event => {
	document.querySelector("#save-button").disabled = true;
});

document.querySelector("#metadata-editor").addEventListener("teian-file-edited", event => {
	document.querySelector("#save-button").disabled = false;
});

document.addEventListener("kuberam.loginElement.events.logout", event => {
	solirom.actions.resetUI();
});

document.addEventListener("teian-file-opened", event => {
});

document.addEventListener("click", async (event) => {
	const target = event.target;
	const composedTarget = event.composedPath()[0];

	// actions to go into the image viewer    
	if (target.matches("#add-scan")) {
		document.getElementById("add-scan-fileupload").click();
	}

	if (target.matches("#replace-scan")) {
		document.getElementById("replace-scan-fileupload").click();
	}

	if (target.matches("#delete-scan")) {
		solirom.actions.deleteScan();
	}

	if (target.matches("#save-button")) {
		await solirom.actions.saveMetadata();
	}

	if (target.matches("#zoom-in-button")) {
		const myImg = document.querySelector("#scan");
		const currWidth = myImg.clientWidth;
		if (currWidth == 5500) {
			return false;
		} else {
			myImg.style.width = (currWidth + 100) + "px";
		}
	}

	if (target.matches("#zoom-out-button")) {
		const myImg = document.querySelector("#scan");
		const currWidth = myImg.clientWidth;
		if (currWidth == 100) {
			return false;
		} else {
			myImg.style.width = (currWidth - 100) + "px";
		}
	}

	if (composedTarget.matches("button.display-scan")) {
		const scanName = composedTarget.getRootNode().host.getAttribute("facs");
		solirom.data.scan.name = scanName;
		solirom.actions.updateImageViewerURL(scanName);
		teian.actions.selectPageBreak(composedTarget);
	}

	// these stay here
	if (composedTarget.matches("button.edit-transcription")) {
		teian.actions.selectPageBreak(composedTarget);

		const dataEditor = document.querySelector("data-editor");
		const pbElement = composedTarget.getRootNode().host;
		dataEditor.currentPageNumberOutput.value = pbElement.getAttribute("n");
		dataEditor.editTranscription(pbElement);
	}
});

document.addEventListener("beforeunload", event => {
	//event.preventDefault();
});

document.addEventListener("change", async (event) => {
	const target = event.target;
	const metadataEditor = document.querySelector("teian-editor#metadata-editor");
	const metadataEditorShadowRoot = metadataEditor.shadowRoot;

	if (target.matches("#add-scan-fileupload")) {
		const commit_email = document.querySelector("sc-user-roles").user.commit_email;

		window.loadingSpinner.show();

		solirom.data.scan.name = solirom.actions.generateNewScanName(solirom.actions.getLatestScanName());
		solirom.data.messages.legalFileSize = "";

		// TODO: why this sorting is needed?
		const files = [...target.files].sort((a, b) => {
			var nameA = a.name.toUpperCase();
			var nameB = b.name.toUpperCase();
			if (nameA < nameB) {
				return -1;
			}
			if (nameA > nameB) {
				return 1;
			}

			return 0;
		});

		for (let file of files) {
			let currentMetadata = metadataEditor.exportDataAsString();
			let [scanRelatedActions, newScanName] = await solirom.actions.getScanRelatedActions(file);

			const payload =
				`
				{
					"branch": "${solirom.data.work.repoDefaultBranch}",
					"commit_message": "${(new Date()).toISOString().split('.')[0] + ", " + commit_email}",
					"author_email": "${commit_email}",
					"actions": [
						${scanRelatedActions}
						{
							"action": "update",
							"file_path": "${solirom.actions.composePath(["data", solirom.data.work.volumeNumber, "index.xml"], "/")}",
							"content": "${solirom.actions.b64EncodeUnicode(metadataEditor.exportDataAsString())}",
							"encoding": "base64"                        
						}                    
					]
				}
			`;
			await fetch(`${solirom.data.work.repoBaseUrl}${solirom.data.work.repoID}/repository/commits`, {
				"headers": {
					"PRIVATE-TOKEN": `${solirom.data.work.repoKey}`,
					"Content-Type": "application/json"
				},
				"method": "POST",
				"body": `${payload}`
			})
				.then(response => {
					if (response.status !== 201) {
						window.loadingSpinner.hide();

						metadataEditor.importData(currentMetadata);

						alert(`Eroare la salvarea scanului ${file.name}. Repetați operația.`);
					}
				});

			solirom.actions.updateImageViewerURL(newScanName);
		}

		setTimeout(() => document.querySelector("#save-button").disabled = true, 100);

		window.loadingSpinner.hide();
		if (solirom.data.messages.legalFileSize !== "") {
			alert("Următoarele fișiere nu au fost salvate deoarece sunt mai mari de 1MB:\n" + solirom.data.messages.legalFileSize);
		}
	}

	if (target.matches("#replace-scan-fileupload")) {
		solirom.data.messages.legalFileSize = "";

		window.loadingSpinner.show();
		const file = target.files[0];

		const isLegalSize = solirom.actions.checkFileSize(file);

		if (!isLegalSize) {
			console.error(error);
			alert(solirom.data.messages.legalFileSize);

			return;
		}

		const commit_email = document.querySelector("sc-user-roles").user.commit_email;
		const imageAsDataURL = await solirom.actions.convertImageFileToWebP(file);
		const currentScanName = solirom.data.scan.name;
		const scanURL = solirom.actions.composePath(["data", solirom.data.work.volumeNumber, currentScanName], "/");

		const payload =
			`
            {
                "branch": "${solirom.data.work.repoDefaultBranch}",
                "commit_message": "${(new Date()).toISOString().split('.')[0] + ", " + commit_email}",
                "author_email": "${commit_email}",
                "content": "${solirom.actions.b64EncodeUnicode(imageAsDataURL)}",
                "encoding": "base64"
            }
        `;

		await fetch(`${solirom.data.work.repoBaseUrl}${solirom.data.work.repoID}/repository/files/${encodeURIComponent(scanURL)}`, {
			"headers": {
				"PRIVATE-TOKEN": `${solirom.data.work.repoKey}`,
				"Content-Type": "application/json"
			},
			"method": "PUT",
			"body": `${payload}`
		})
			.then(response => {
				if (response.status !== 200) {
					window.loadingSpinner.hide();

					alert(`Eroare la salvarea scanului ${file.name}. Repetați operația.`);
				}
			});

		const currentPbElement = metadataEditorShadowRoot.querySelector("#content *[data-name = '" + solirom.data.work.textSection + "'] *[data-name = 'pb'][facs = '" + currentScanName + "']");
		teian.actions.selectPageBreak(currentPbElement);

		solirom.actions.updateImageViewerURL(currentScanName);
		window.loadingSpinner.hide();
	}

	if (target.matches("input[name = 'sectionSelectorButton']")) {
		const textSection = target.value;

		solirom.data.work.textSection = textSection;

		[...metadataEditorShadowRoot.querySelectorAll("#content *[data-name = 'text'] > *")].forEach((section) => section.style.display = 'none');
		metadataEditorShadowRoot.querySelector("#content *[data-name = '" + solirom.data.work.textSection + "']").style.display = 'inline';
	}

	if (target.matches("#volume-selector")) {
		const volumeNumber = target.value;

		if (volumeNumber === "") {
			return;
		}
		solirom.data.work.volumeNumber = volumeNumber;
		const volumeMetadataFilePath = encodeURIComponent(solirom.actions.composePath(["data", volumeNumber, "index.xml"], "/"));
		solirom.data.workMetadataUrl = volumeMetadataFilePath;

		let contents = await fetch(`${solirom.data.work.repoBaseUrl}${solirom.data.work.repoID}/repository/files/${volumeMetadataFilePath}/raw`, {
			"headers": {
				"PRIVATE-TOKEN": `${solirom.data.work.repoKey}`,
				'If-None-Match': ''
			}
		})
			.then(response => {
				if (response.status !== 200) {
					alert(Messages.errorVolumeMetadata);

					throw new Error(Messages.errorVolumeMetadata);
				} else {
					return response.text();
				}
			});


		metadataEditor.importData(contents);

		[...metadataEditorShadowRoot.querySelectorAll("#content *[data-name = 'text'] > *")].forEach((section) => section.style.display = "none");
		metadataEditorShadowRoot.querySelector("#content *[data-name = '" + solirom.data.work.textSection + "']").style.display = "inline";

		document.querySelector("#scan").src = "";
		document.querySelector("#add-scan").disabled = false;
		document.querySelector("#replace-scan").disabled = false;
		document.querySelector("#zoom-in-button").disabled = false;
		document.querySelector("#zoom-out-button").disabled = false;

		setTimeout(() => document.querySelector("#save-button").disabled = true, 100);
	}
});

solirom.actions.saveMetadata = async () => {
	const commit_email = document.querySelector("sc-user-roles").user.commit_email;
	if (commit_email === "") {
		alert("Selectați un utilizator.");

		return;
	}

	window.loadingSpinner.show();

	const metadataEditor = document.querySelector("teian-editor#metadata-editor");
	const metadataFilePath = solirom.actions.composePath(["data", solirom.data.work.volumeNumber, "index.xml"], "/");

	const currentMetadata = metadataEditor.exportDataAsString();

	const payload =
		`
		{
			"branch": "${solirom.data.work.repoDefaultBranch}",
			"commit_message": "${(new Date()).toISOString().split('.')[0] + ", " + commit_email}",
			"author_email": "${commit_email}",
			"content": "${solirom.actions.b64EncodeUnicode(currentMetadata)}",
			"encoding": "base64"
		}
	`;	// + "[ci skip]"
	await fetch(`${solirom.data.work.repoBaseUrl}${solirom.data.work.repoID}/repository/files/${encodeURIComponent(metadataFilePath)}`, {
		"headers": {
			"PRIVATE-TOKEN": `${solirom.data.work.repoKey}`,
			"Content-Type": "application/json"
		},
		"method": "PUT",
		"body": `${payload}`
	})
		.then(response => {
			if (response.status !== 200) {
				window.loadingSpinner.hide();

				metadataEditor.importData(currentMetadata);

				alert("Lucrarea nu poate fi salvată. Repetați operația.");
			}
		});

	window.loadingSpinner.hide();
	setTimeout(() => document.querySelector("#save-button").disabled = true, 100);
};

solirom.actions.getLatestScanName = () => {
	const scanNames = [...document.querySelector("teian-editor#metadata-editor").shadowRoot.querySelectorAll("#content *[data-name = 'pb']")].map((element) => element.getAttribute("facs")).sort();

	if (scanNames.length === 0) {
		return "f0000.b64"
	} else {
		return scanNames[scanNames.length - 1];
	}
};

solirom.actions.generateNewScanName = (latestScanName) => {
	const volumeNumber = solirom.data.work.volumeNumber;

	latestScanName = latestScanName.match(/\d+/)[0];
	if (volumeNumber != "") {
		latestScanName = latestScanName.replace(volumeNumber + "/", "");
	}
	latestScanName = parseInt(latestScanName);

	var newScanName = latestScanName + 1;
	if (String(newScanName).match("[6]{3}") !== null) {
		newScanName++;
	}
	newScanName = "f" + String(newScanName).padStart(4, '0') + ".b64";
	/* 	if (volumeNumber != "") {
			newScanName = volumeNumber + "/" + newScanName;	
		}  */

	return newScanName;
};

solirom.actions.updateImageViewerURL = async (scanRelativeURL) => {
	window.loadingSpinner.show();

	const imageElement = document.querySelector("#scan");

	// case when the `scanRelativeURL` is absolute
	if (scanRelativeURL.startsWith("https://")) {
		imageElement.src = scanRelativeURL;
	} else {
		// case when the `scanRelativeURL` is relative
		let scanRelativeURLencoded = encodeURIComponent(solirom.actions.composePath(["data", solirom.data.work.volumeNumber, scanRelativeURL], "/"));
		let scanURL = `${solirom.data.work.repoBaseUrl}${solirom.data.work.repoID}/repository/files/${scanRelativeURLencoded}/raw`;

		let scanAsDataURL = await fetch(scanURL, {
			"mode": "cors",
			"headers": {
				"PRIVATE-TOKEN": `${solirom.data.work.repoKey}`,
				'If-None-Match': ''
			}
		})
			.then(response => {
				if (response.status !== 200) {
					alert(Messages.errorScan);

					throw new Error(Messages.errorScan);
				} else {
					return response.text();
				}
			});

		const imageParentElement = imageElement.parentElement;
		imageElement.remove();
		imageParentElement.insertAdjacentHTML("beforeend", solirom.data.templates.img(scanAsDataURL));
	}

	document.querySelector("#replace-scan").disabled = false;
	document.querySelector("#delete-scan").disabled = false;
	document.querySelector("#zoom-in-button").disabled = false;
	document.querySelector("#zoom-out-button").disabled = false;

	window.loadingSpinner.hide();
};

solirom.actions.checkFileSize = (file) => {
	var isLegalSize = true;

	const fileSize = (file.size / (1024 * 1024)).toFixed(2);

	if (fileSize > 1) {
		solirom.data.messages.legalFileSize += file.name + "\n";
		isLegalSize = false;
	}

	return isLegalSize;
};

solirom.actions.getScanRelatedActions = async (file) => {
	const isLegalSize = solirom.actions.checkFileSize(file);

	if (!isLegalSize) {
		return "";
	}

	const scanAsDataURL = await solirom.actions.convertImageFileToWebP(file);

	let newScanName = "scans/" + solirom.data.scan.name;
	solirom.data.scan.name = solirom.actions.generateNewScanName(newScanName);
	const scanURL = solirom.actions.composePath(["data", solirom.data.work.volumeNumber, newScanName], "/");

	const newTranscriptionName = "t" + newScanName.match(/\d+/)[0] + ".xml";
	const newTranscriptionPath = solirom.actions.composePath(["data", solirom.data.work.volumeNumber, solirom.data.repos.cflr.transcriptionsPath, newTranscriptionName], "/");

	const textSection = document.querySelector("teian-editor#metadata-editor").shadowRoot.querySelector("#content *[data-name = '" + solirom.data.work.textSection + "']");
	textSection.insertAdjacentHTML("beforeend", solirom.data.templates.pb({ "facs": newScanName, "transcriptionPath": solirom.actions.composePath([solirom.data.repos.cflr.transcriptionsPath, newTranscriptionName], "/") }));
	teian.actions.selectPageBreak(textSection.lastElementChild);

	const actions =
		`
		{
			"action": "create",
			"file_path": "${scanURL}",
			"content": "${solirom.actions.b64EncodeUnicode(scanAsDataURL)}",
			"encoding": "base64"                        
		},
		{
			"action": "create",
			"file_path": "${newTranscriptionPath}",
			"content": "${solirom.actions.b64EncodeUnicode(solirom.data.templates.transcriptionFile)}",
			"encoding": "base64"                        
		},
	`;

	return [actions, newScanName];
};

/**
 * Deletes the selected scan, transcription, entries, and updates the work's / volume's metadata
 * @public
 * @return void
 */
solirom.actions.deleteScan = async () => {
	var confirmMsg = confirm("Ștergeți scanul? Se va șterge și transcrierea scanului.");

	if (confirmMsg) {
		const commit_email = document.querySelector("sc-user-roles").user.commit_email;
		const scanName = solirom.data.scan.name;
		const scanURL = solirom.actions.composePath(["data", solirom.data.work.volumeNumber, scanName], "/");

		window.loadingSpinner.show();

		// get the old state
		const metadataEditor = document.querySelector("teian-editor#metadata-editor");
		let currentMetadata = metadataEditor.exportDataAsString();

		// set the new state
		const transcriptionName = "t" + scanName.match(/\d+/)[0] + ".xml";
		const transcriptionPath = solirom.actions.composePath(["data", solirom.data.work.volumeNumber, solirom.data.repos.cflr.transcriptionsPath, transcriptionName], "/");

		metadataEditor.shadowRoot.querySelector("#content *[data-name = 'pb'][facs = '" + scanName + "']").remove();

		const transcription = await solirom.actions._getTranscription(transcriptionPath);
		const transcriptionDocument = (new DOMParser()).parseFromString(transcription, "application/xml").documentElement;
		const entryRelatedActions = solirom.actions.getEntryRelatedActions(transcriptionDocument);
		let newMetadata = metadataEditor.exportDataAsString();

		// save the new state
		const payload =
			`
			{
				"branch": "${solirom.data.work.repoDefaultBranch}",
				"commit_message": "${(new Date()).toISOString().split('.')[0] + ", " + commit_email}",
				"author_email": "${commit_email}",
				"actions": [
					${entryRelatedActions}
                    {
                        "action": "delete",
                        "file_path": "${scanURL}"                        
                    },
                    {
                        "action": "delete",
                        "file_path": "${transcriptionPath}"                        
                    },
					{
						"action": "update",
						"file_path": "${solirom.actions.composePath(["data", solirom.data.work.volumeNumber, "index.xml"], "/")}",
						"content": "${solirom.actions.b64EncodeUnicode(newMetadata)}",
						"encoding": "base64"
					}
				]
			}
		`;

		await fetch(`${solirom.data.work.repoBaseUrl}${solirom.data.work.repoID}/repository/commits`, {
			"headers": {
				"PRIVATE-TOKEN": `${solirom.data.work.repoKey}`,
				"Content-Type": "application/json"
			},
			"method": "POST",
			"body": `${payload}`
		})
			.then(response => {
				if (response.status !== 201) {
					window.loadingSpinner.hide();

					// restore the old state
					metadataEditor.importData(currentMetadata);

					alert(`Eroare la ștergerea transcrierii ${file.name} și a fișierelor asociate. Repetați operația.`);
				}
			});

		document.querySelector("#scan").src = "";

		window.loadingSpinner.hide();
	}
};

solirom.actions.getEntryRelatedActions = (transcriptionDocument) => {
	let entryHrefs = [...transcriptionDocument.querySelectorAll("*|include")].map(element => element.getAttribute("href"));
	entryHrefs = [...new Set(entryHrefs)];

	return entryHrefs.map((entryHref) => {
		const entryPath = solirom.actions.composePath(["data", solirom.data.work.volumeNumber, solirom.data.repos.cflr.transcriptionsPath, entryHref], "/");

		return `
			{
				"action": "delete",
				"file_path": "${entryPath}"                        
			},			
		`;
	}).join("");
};

solirom.actions._getTranscription = async (path) => {
	let contents;

	try {
		contents = await fetch(`${solirom.data.work.repoBaseUrl}${solirom.data.work.repoID}/repository/files/${encodeURIComponent(path)}/raw`, {
			"headers": {
				"PRIVATE-TOKEN": `${solirom.data.work.repoKey}`,
				'If-None-Match': ''
			}
		})
			.then(response => {
				if (response.status !== 200) {
					alert(Messages.errorTranscriptionGet);

					throw new Error(Messages.errorTranscriptionGet);
				} else {
					return response.text();
				}
			});
	} catch (error) {
		console.error(error);

		window.loadingSpinner.hide();

		alert("Eroare la încărcarea transcrierii.");

		return;
	}

	return contents;
};

solirom.actions.composePath = (steps, separator) => {
	return steps.filter(Boolean).join(separator);
};

solirom.actions.displayMetadataEditor = () => {
	document.querySelector("#metadata-editor").style.display = "inline-block";
	document.querySelector("#data-editor").style.display = "none";
};

solirom.actions.displayDataEditor = () => {
	document.querySelector("#metadata-editor").style.display = "none";
	document.querySelector("#data-editor").style.display = "inline-block";
};
solirom.actions.resetUI = () => {
	document.querySelector("teian-editor#metadata-editor").reset();
	const dataEditor = document.querySelector("data-editor");
	dataEditor.transcriptionEditor.reset();
	dataEditor.entryEditor.reset();
	solirom.actions.resetScanViewer();
	solirom.actions.displayMetadataEditor();
};
//start functions related to the image viewer
solirom.actions.resetScanViewer = () => {
	document.querySelector("#scan").src = "";
	document.querySelector("#add-scan").disabled = true;
	document.querySelector("#replace-scan").disabled = true;
	document.querySelector("#delete-scan").disabled = true;
	document.querySelector("#zoom-in-button").disabled = true;
	document.querySelector("#zoom-out-button").disabled = true;
};
solirom.actions.convertImageFileToWebP = async (file) => {

	const imageAsDataURL = await new Promise((resolve, reject) => {
		let rawImage = new Image();

		rawImage.addEventListener("load", () => {
			resolve(rawImage);
		});

		rawImage.src = URL.createObjectURL(file);
	}).
		then((rawImage) => {
			return new Promise((resolve, reject) => {
				let canvas = document.createElement("canvas");
				let ctx = canvas.getContext("2d");

				canvas.width = rawImage.width;
				canvas.height = rawImage.height;
				ctx.drawImage(rawImage, 0, 0);

				resolve(canvas.toDataURL("image/webp", 1));
			});
		});

	var convertedImgSize = Math.round(imageAsDataURL.length * 3 / 4);
	convertedImgSize = (convertedImgSize / (1024 * 1024)).toFixed(3);
	const originalImgSize = (file.size / (1024 * 1024)).toFixed(3);
	console.log(`${file.name}: ${originalImgSize} MB > ${convertedImgSize} MB`);

	return imageAsDataURL;
};
//end functions related to the image viewer

export default solirom;