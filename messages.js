export let errorRepoBranch = "Datele despre depozitul cu datele lucrării nu pot fi încărcate.";

export let errorVolumes = "Datele despre volumele lucrării nu pot fi încărcate.";

export let errorGeneralMetadata = "Metadatele lucrării nu pot fi încărcate.";

export let errorVolumeMetadata = "Metadatele volumului nu pot fi încărcate.";

export let errorScan = "Scanul nu poate fi încărcat.";

export let errorTranscriptionGet = "Transcrierea nu poate fi încărcată.";
